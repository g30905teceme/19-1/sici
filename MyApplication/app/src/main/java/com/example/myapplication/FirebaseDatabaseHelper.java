package com.example.myapplication;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.firebase.database.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;

public class FirebaseDatabaseHelper {

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;
    private List<DataSnapshot> arr = new ArrayList<>();
    private final Handler handler = new Handler();
    private Runnable mRunnable;

    public interface DataStatus {
        void DataIsLoaded(List<DataSnapshot> data);
    }

    public FirebaseDatabaseHelper() {
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference("data");
    }

    public void getData(final DataStatus dataStatus) {
        mReference.limitToLast(10).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int i = 0;
                arr.clear();
                for (DataSnapshot iter : dataSnapshot.getChildren()) {
                    arr.add(iter);
                    i++;
                }
                dataStatus.DataIsLoaded(arr);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}


