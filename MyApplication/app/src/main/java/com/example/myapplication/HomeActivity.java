package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import java.util.List;
import android.content.Intent;
import android.net.Uri;
import android.provider.Telephony;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import android.os.Handler;

public class HomeActivity extends AppCompatActivity {

    private EditText txtMobile;
    private EditText txtMessage;
    private Button btnSms;
    private final Handler handler = new Handler();
    private Runnable mRunnable;
    private boolean send = false;
    private int cont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        new Handler().postDelayed(new Runnable() {
            int umbral = 60;
            @Override
            public void run() {
                new FirebaseDatabaseHelper().getData(new FirebaseDatabaseHelper.DataStatus() {
                    @Override
                    public void DataIsLoaded(List<DataSnapshot> data) {
                        cont = 0;
                        for (DataSnapshot index :  data) {
                            System.out.println("Value -> :"+ index );
                            if((Long)index.getValue() > umbral) {
                                send = true;
                                cont++;
                            }
                        }
                    }

                });

                handler.postDelayed(this, 13000);
            }
        }, 13000);

        if(send && cont == 10) {
            sendSMS();
            System.out.println("Test");
        }
    }

    public void sendSMS() {
        String numero_telefono = "";
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(numero_telefono, null, "Se detecto humo", null,null);

    }

}
