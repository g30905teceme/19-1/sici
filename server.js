
// Conexión con firebase

var firebase = require("firebase");

var gcm = require('node-gcm');

var config = {
    apiKey: "AIzaSyDtTxmsIC3mGOiUdc-aveAh1P5M0lBE8mg",
    authDomain: "iotproject-6a5b7.firebaseapp.com",
    databaseURL: "https://iotproject-6a5b7.firebaseio.com",
    projectId: "iotproject-6a5b7",
    storageBucket: "iotproject-6a5b7.appspot.com",
    messagingSenderId: "806532593308",
  };


firebase.initializeApp(config);
var ref = firebase.database().ref('data/');

var umbral = 32;
var cont = 0;
var send= false;

//Monitoreo de datos
(function(){
    ref.limitToLast(10).on("value",function(snapshot) {
        snapshot.forEach(function(data) {
            console.log("Valor de voltaje -> ",data.val());
            if (data.val()>umbral) {
                cont = cont+1;
                send = true;
            } else {
                send = false;
            }
         });
        console.log("-------------------------");
        if (send == true && cont == 10) {
            exports.enviarNotificaciones();
            console.log("mensaje enviado");
        }
        cont =0;
    });
    setTimeout(arguments.callee, 10000);
})();

exports.enviarNotificaciones = function(){

    var sender = new gcm.Sender('AIzaSyBXCekfCP5wp6aRfuJxW-BSMBAHeiXJrkQ'); //Obetnido en la consola de mensajeria de firebase
    var message = new gcm.Message({
        notification: {
            title: "Alerta!",
            body: "Se detecto humo en la habitación" 
        }
    });

    var regTokens = [];
    regTokens.push('Token del dispositivo'); //Obtenido mediante la conexion en android.

    sender.send(message, { registrationTokens: regTokens }, function(err,response) {
        if(err){
            console.error(err);
        } else {
            console.log(response);
        }
    });

};